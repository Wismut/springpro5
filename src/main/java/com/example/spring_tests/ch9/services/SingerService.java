package com.example.spring_tests.ch9.services;

import com.example.spring_tests.ch9.entities.Singer;

public interface SingerService {
    Singer save(Singer singer);

    long count();
}
