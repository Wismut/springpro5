package com.example.spring_tests.ch5;

public class Guitar {
    public String play() {
        return "G C G C Am D7";
    }
}
