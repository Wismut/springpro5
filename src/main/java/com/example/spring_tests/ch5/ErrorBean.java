package com.example.spring_tests.ch5;

public class ErrorBean {
    public void errorProneMethod() throws Exception {
        System.out.println("123");
        throw new Exception("Generic Exception");
    }

    public void otherErrorProneMethod() throws IllegalArgumentException {
        throw new IllegalArgumentException("IllegalArgument Exception");
    }
}
