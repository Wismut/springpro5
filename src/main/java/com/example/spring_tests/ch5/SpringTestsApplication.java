package com.example.spring_tests.ch5;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.Resource;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//@SpringBootApplication
public class SpringTestsApplication {

    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfigThree.class);
        MessageRenderer renderer = context.getBean(MessageRenderer.class);
        renderer.render();

//        List<File> list = getFiles(System.getProperty("java.class.path"));
//        for (File file : list) {
//            System.out.println(file.getPath());
//        }

//        ApplicationContext ctx = new ClassPathXmlApplicationContext();
//        File file = File.createTempFile("test", "txt");
//        file.deleteOnExit();
//        Resource res1 = ctx.getResource("file:/" + file.getPath());
//        displayInfo(res1);
//        Resource res2 = ctx.getResource("classpath:/test.txt");
//        displayInfo(res2);
//        Resource res3 = ctx.getResource("http://www.google.com");
//        displayInfo(res3);

//        SpringApplication.run(SpringTestsApplication.class, args);
    }

    private static void displayInfo(Resource res) throws Exception {
        System.out.println(res.getClass());
        System.out.println(res.getURL().getContent());
        System.out.println("");
    }

    public static List<File> getFiles(String paths) {
        List<File> filesList = new ArrayList<>();
        for (final String path : paths.split(File.pathSeparator)) {
            final File file = new File(path);
            if (file.isDirectory()) {
                recurse(filesList, file);
            } else {
                filesList.add(file);
            }
        }
        return filesList;
    }

    private static void recurse(List<File> filesList, File f) {
        File list[] = f.listFiles();
        for (File file : list) {
            if (file.isDirectory()) {
                recurse(filesList, file);
            } else {
                filesList.add(file);
            }
        }
    }

}
