package com.example.spring_tests.ch5;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"com.example.spring_tests.ch5"})
public class AppConfigFour {

}
