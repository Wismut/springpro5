package com.example.spring_tests.ch5;

public interface SimpleBean {
    void advised();

    void unadvised();
}
