package com.example.spring_tests.ch5;

public interface MessageProvider {
    String getMessage();
}
