package com.example.spring_tests.ch5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.example.spring_tests"})
public class AppConfigTwo {
    @Autowired
    MessageProvider provider;

    @Bean(name = "messageRenderer")
    public MessageRenderer messageRenderer() {
        MessageRenderer renderer =
                new StandardOutMessageRenderer();
        renderer.setMessageProvider(provider);
        return renderer;
    }
}
