package com.example.spring_tests.ch5;

public interface Singer {
    void sing();
}
