package com.example.spring_tests.ch11.repos;

import com.example.spring_tests.ch11.entities.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
}
