package com.example.spring_tests.ch11.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
//@EnableScheduling
@EnableAsync
@Import({DataServiceConfig.class})
//@ImportResource("classpath:spring/task-namespace-app-context.xml")
public class AppConfig {
    //    @Bean
//    TaskScheduler carScheduler() {
//        ThreadPoolTaskScheduler carScheduler =
//                new ThreadPoolTaskScheduler();
//        carScheduler.setPoolSize(10);
//        return carScheduler;
//    }

    @Bean
    TaskExecutor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }
}
