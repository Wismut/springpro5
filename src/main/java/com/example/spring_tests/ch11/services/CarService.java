package com.example.spring_tests.ch11.services;

import com.example.spring_tests.ch11.entities.Car;

import java.util.List;

public interface CarService {
    List<Car> findAll();

    Car save(Car car);

    void updateCarAgeJob();

    boolean isDone();
}
