package com.example.spring_tests.ch7.dao;

import com.example.spring_tests.ch7.entities.Singer;

import java.util.List;

public interface SingerDao {
    List<Singer> findAll();

    List<Singer> findAllWithAlbum();

    Singer findById(Long id);

    Singer save(Singer contact);

    void delete(Singer contact);
}
