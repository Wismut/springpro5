package com.example.spring_tests.ch15.services;

import com.example.spring_tests.ch15.entities.Singer;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class SingerService {
    public List<Singer> findAll() {
        return Collections.singletonList(new Singer("name", "ln"));
    }

    public List<Singer> findByFirstNameAndLastName(String firstName, String lastName) {
        return Collections.singletonList(new Singer(firstName, lastName));
    }
}
