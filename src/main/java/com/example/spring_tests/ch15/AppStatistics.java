package com.example.spring_tests.ch15;

public interface AppStatistics {
    int getTotalSingerCount();
}
