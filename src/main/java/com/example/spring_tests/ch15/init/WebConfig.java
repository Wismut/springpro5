package com.example.spring_tests.ch15.init;

import com.example.spring_tests.ch15.AppStatistics;
import com.example.spring_tests.ch15.AppStatisticsImpl;
import com.example.spring_tests.ch15.CustomStatistics;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jmx.export.MBeanExporter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

//@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.example.spring_tests.ch15"})
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Bean
    AppStatistics appStatisticsBean() {
        return new AppStatisticsImpl();
    }

    @Bean
    CustomStatistics statisticsBean() {
        return new CustomStatistics();
    }

    @Bean
    SessionFactory sessionFactory() {
        return entityManagerFactory.unwrap(SessionFactory.class);
    }

    @Bean
    MBeanExporter jmxExporter() {
        MBeanExporter exporter = new MBeanExporter();
        Map<String, Object> beans = new HashMap<>();
        beans.put("bean:name=ProSpring5SingerApp", appStatisticsBean());
        beans.put("bean:name=Prospring5SingerApp-hibernate", statisticsBean());
        exporter.setBeans(beans);
        return exporter;
    }
}
