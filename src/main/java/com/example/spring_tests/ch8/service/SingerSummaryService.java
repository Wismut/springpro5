package com.example.spring_tests.ch8.service;

import com.example.spring_tests.ch8.view.SingerSummary;

import java.util.List;

public interface SingerSummaryService {
    List<SingerSummary> findAll();
}
