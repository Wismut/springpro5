package com.example.spring_tests.ch8.services;

import com.example.spring_tests.ch8.entities.SingerAudit;

import java.util.List;

public interface SingerAuditService {
    List<SingerAudit> findAll();

    SingerAudit findById(Long id);

    SingerAudit save(SingerAudit singer);

    SingerAudit findAuditByRevision(Long id, int revision);
}
