package com.example.spring_tests.ch8.services;

import com.example.spring_tests.ch8.entities.Album;
import com.example.spring_tests.ch8.entities.Singer;

import java.util.List;

public interface AlbumService {
    List<Album> findBySinger(Singer singer);

    List<Album> findByTitle(String title);
}
