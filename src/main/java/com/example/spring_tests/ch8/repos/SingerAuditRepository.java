package com.example.spring_tests.ch8.repos;

import com.example.spring_tests.ch8.entities.SingerAudit;
import org.springframework.data.repository.CrudRepository;

public interface SingerAuditRepository extends CrudRepository<SingerAudit, Long> {

}
