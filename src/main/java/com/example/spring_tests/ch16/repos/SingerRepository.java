package com.example.spring_tests.ch16.repos;

import com.example.spring_tests.ch16.entities.Singer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SingerRepository extends PagingAndSortingRepository<Singer, Long> {

}
