package com.example.spring_tests.ch14;

public interface RuleEngine {
    void run(Rule rule, Object object);
}
