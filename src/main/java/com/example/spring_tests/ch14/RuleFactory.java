package com.example.spring_tests.ch14;

public interface RuleFactory {
    Rule getAgeCategoryRule();
}
