package com.example.spring_tests.ch14;

public interface SingerService {
    void applyRule(Singer singer);
}
