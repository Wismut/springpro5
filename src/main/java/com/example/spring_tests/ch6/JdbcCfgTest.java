package com.example.spring_tests.ch6;

import com.example.spring_tests.ch6.config.DbConfig;
import com.example.spring_tests.ch6.config.EmbeddedJdbcConfig;
import com.example.spring_tests.ch6.dao.SingerDao;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class JdbcCfgTest {
    @Test
    public void testH2DB() {
        AnnotationConfigApplicationContext ctx =
                new AnnotationConfigApplicationContext(EmbeddedJdbcConfig.class);
//        ctx.load("classpath:spring/embedded-h2-cfg.xml");
//        ctx.refresh();
        testDao(ctx.getBean(SingerDao.class));
        ctx.close();
    }

    private void testDao(SingerDao singerDao) {
        assertNotNull(singerDao);
        String singerName = singerDao.findNameById(1L);
        assertTrue("John Mayer".equals(singerName));
    }
}
