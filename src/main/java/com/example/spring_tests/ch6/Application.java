package com.example.spring_tests.ch6;

import com.example.spring_tests.ch6.dao.SingerDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

//@SpringBootApplication
public class Application {
    private static Logger logger =
            LoggerFactory.getLogger(Application.class);

    public static void main(String... args) throws Exception {
        ConfigurableApplicationContext ctx =
                SpringApplication.run(Application.class, args);
        SingerDao singerDao = ctx.getBean(SingerDao.class);
        String singerName = singerDao.findNameById(1L);
        logger.info("Retrieved singer: " + singerName);
        ctx.close();
    }
}
