package com.example.spring_tests.ch12;

public interface WeatherService {
    String getForecast(String stateCode);
}
