package com.example.spring_tests.ch12;

public interface MessageSender {
    void sendMessage(String message);
}
