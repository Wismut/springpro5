package com.example.spring_tests.ch12;

import org.springframework.boot.SpringApplication;

//@SpringBootApplication
public class ApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationMain.class);
    }
}
