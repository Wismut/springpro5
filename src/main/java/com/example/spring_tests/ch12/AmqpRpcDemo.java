//package com.example.spring_tests.ch12;
//
//import com.example.spring_tests.ch12.config.RabbitMQConfig;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
////import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.context.support.GenericApplicationContext;
//
//import java.io.IOException;
//
//public class AmqpRpcDemo {
//    private static Logger logger = LoggerFactory.getLogger(AmqpRpcDemo.class);
//
//    public static void main(String... args) throws IOException {
//        GenericApplicationContext ctx =
//                new AnnotationConfigApplicationContext(RabbitMQConfig.class);
//        RabbitTemplate rabbitTemplate = ctx.getBean(RabbitTemplate.class);
//        rabbitTemplate.convertAndSend("FL");
//        rabbitTemplate.convertAndSend("MA");
//        rabbitTemplate.convertAndSend("CA");
//        System.in.read();
//        ctx.close();
//    }
//}
