package com.example.spring_tests.ch12;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

//@SpringBootApplication(scanBasePackages = "com.example.spring_tests.ch12")
public class App {
    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws IOException {
        ConfigurableApplicationContext ctx =
                SpringApplication.run(App.class, args);
        assert (ctx != null);
        logger.info("Application Started ...");
        System.in.read();
        ctx.close();
    }
}
