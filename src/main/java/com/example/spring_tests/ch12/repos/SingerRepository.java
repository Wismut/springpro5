package com.example.spring_tests.ch12.repos;

import com.example.spring_tests.ch12.entities.Singer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SingerRepository extends CrudRepository<Singer, Long> {
    List<Singer> findByFirstName(String firstName);

    Singer findByFirstNameAndLastName(String firstName, String lastName);
}
