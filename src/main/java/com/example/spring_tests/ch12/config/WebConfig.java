package com.example.spring_tests.ch12.config;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

}
