package com.example.spring_tests.ch13.repos;

import com.example.spring_tests.ch13.entities.Singer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

//@Repository
public interface SingerRepository extends CrudRepository<Singer, Long> {
    List<Singer> findByFirstName(String firstName);

    Singer findByFirstNameAndLastName(String firstName, String lastName);
}
